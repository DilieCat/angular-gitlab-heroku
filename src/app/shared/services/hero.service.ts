import { IHero } from '../models/hero'
import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { environment } from '../../../environments/environment'
import { Hero } from '../models/heromodel'
import { Weapon } from '../models/weapon'

@Injectable({
  providedIn: 'root'
})
export class HeroService {
  private url = environment.apiBaseUrl + 'hero'

  public classes = ['Ranger', 'Sniper', 'Warrior', 'Shaman']

  constructor(private http: HttpClient) {}

  getHero(): Observable<IHero[]> {
    return this.http.get<IHero[]>(this.url)
  }

  getOneHero(id: string): Observable<Hero[]> {
    return this.http.get<Hero[]>(this.url + '/' + id)
  }

  getUserHero(id: string): Observable<IHero[]> {
    return this.http.get<IHero[]>(this.url + '/user/' + id)
  }

  getHeroExceptFromUser(id: string) {
    return this.http.get<IHero[]>(this.url + '/notFromUser/' + id)
  }

  deleteHero(id: string) {
    return this.http.delete<IHero>(this.url + '/' + id)
  }

  addHero(hero: Hero, id: string): Observable<Hero> {
    return this.http.post<Hero>(this.url + '/' + id, hero)
  }

  editHero(hero: Hero): Observable<Hero> {
    return this.http.put<Hero>(this.url + '/' + hero._id, hero)
  }

  addWeapon(weapon: Weapon, id: string) {
    console.log(this.http.post(this.url + '/' + id + '/weapon', weapon).subscribe((data) => console.log(data)))
    return this.http.post(this.url + '/' + id + '/weapon', weapon)
  }
}
