import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { environment } from '../../../environments/environment'
import { Battleground } from '../models/battleground'
import { BattlegroundForm } from '../models/battlegroundForm'

@Injectable({
  providedIn: 'root'
})
export class BattlegroundService {
  private url = environment.apiBaseUrl + 'battleground'
  public condition = ['Rainy', 'Sunny', 'Snowing', 'Dry air', 'Middle of city']

  constructor(private http: HttpClient) {}

  getBattlegrounds(): Observable<any> {
    return this.http.get(this.url)
  }

  addBattleground(battlegroundForm: BattlegroundForm): Observable<any> {
    return this.http.post(this.url, battlegroundForm)
  }
}
