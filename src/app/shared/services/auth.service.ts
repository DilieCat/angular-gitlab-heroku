import { Injectable } from '@angular/core'
import { User } from '../models/user'
import { Observable, throwError } from 'rxjs'
import { catchError, map } from 'rxjs/operators'
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http'
import { Router } from '@angular/router'
import { environment } from 'src/environments/environment'

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  endpoint: string = environment.apiBaseUrl
  headers = new HttpHeaders().set('Content-Type', 'application/json')
  currentUser = localStorage.getItem('user_id')

  constructor(private http: HttpClient, public router: Router) {}

  // Sign-up
  signUp(user: User): Observable<any> {
    const api = `${this.endpoint}auth/register-user`
    return this.http.post(api, user).pipe(catchError(this.handleError))
  }

  // Sign-in
  signIn(user: User) {
    return this.http.post<any>(`${this.endpoint}auth/signin`, user)
  }

  getToken() {
    return localStorage.getItem('access_token')
  }

  get isLoggedIn(): boolean {
    const authToken = localStorage.getItem('access_token')
    return authToken !== null ? true : false
  }

  doLogout() {
    localStorage.removeItem('access_token')
    const removeToken = localStorage.getItem('acces_token')
    if (removeToken == null) {
      this.router.navigate(['log-in'])
    }
  }

  // User profile
  getUserProfile(id): Observable<any> {
    const api = `${this.endpoint}auth/user-profile/${id}`
    return this.http.get(api, { headers: this.headers }).pipe(
      map((res: Response) => {
        return res || {}
      }),
      catchError(this.handleError)
    )
  }

  // Error
  handleError(error: HttpErrorResponse) {
    let msg = ''
    if (error.error instanceof ErrorEvent) {
      // client-side error
      msg = error.error.message
    } else {
      // server-side error
      msg = `Error Code: ${error.status}\nMessage: ${error.message}`
    }
    return throwError(msg)
  }
}
