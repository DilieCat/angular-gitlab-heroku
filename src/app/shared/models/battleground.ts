export class Battleground {
  constructor(
    public name: string,
    public heroes: any[],
    public condition: string,
    public heroWon: {},
    public winMessage: string
  ) {}
}
