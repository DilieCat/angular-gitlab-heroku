import { HeroAddComponent } from './hero-add/hero-add.component'
import { HeroDeleteConfirmationComponent } from './hero-delete-confirmation/hero-delete-confirmation.component'
import { HeroService } from '../shared/services/hero.service'
import { Component, OnInit } from '@angular/core'
import { faTrashAlt, faEdit } from '@fortawesome/free-solid-svg-icons'
import { IHero } from '../shared/models/hero'
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { HeroEditComponent } from './hero-edit/hero-edit.component'
import { ActivatedRoute } from '@angular/router'
import { AuthService } from '../shared/services/auth.service'

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.scss']
})
export class HeroComponent implements OnInit {
  faTrashAlt = faTrashAlt
  faEdit = faEdit
  public heroes = []
  currentUserId = this.authServe.currentUser

  constructor(
    private heroService: HeroService,
    private modalService: NgbModal,
    private actRoute: ActivatedRoute,
    private authServe: AuthService
  ) {}

  ngOnInit() {
    this.heroService.getUserHero(this.currentUserId).subscribe(data => (this.heroes = data))
  }

  onClickDelete(hero: IHero) {
    const modalRef = this.modalService.open(HeroDeleteConfirmationComponent)
    modalRef.componentInstance.hero = hero

    modalRef.result.then(
      () => {
        this.heroes = this.heroes.filter(h => h !== hero)
        this.heroService.deleteHero(hero._id).subscribe()
      },
      () => {}
    )
  }

  onClickAdd() {
    const modalRef = this.modalService.open(HeroAddComponent)

    modalRef.result.then(
      hero => {
        if (localStorage.getItem('access_token') !== undefined) {
          this.heroService.addHero(hero, this.currentUserId).subscribe(newHero => this.heroes.push(newHero))
        }
      },
      () => {}
    )
  }

  open() {
    this.modalService.open(HeroDeleteConfirmationComponent)
  }

  onClickEdit(hero) {
    const modalRef = this.modalService.open(HeroEditComponent)
    modalRef.componentInstance.hero = hero

    modalRef.result.then(
      editedHero => {
        this.heroService.editHero(editedHero).subscribe(newHero => {
          const foundIndex = this.heroes.findIndex(x => x._id === newHero._id)
          this.heroes[foundIndex] = newHero
        })
      },
      () => {}
    )
  }
}
