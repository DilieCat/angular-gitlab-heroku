import { HeroComponent } from './hero/hero.component'
import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { UsecasesComponent } from './about/usecases/usecases.component'
import { DashboardComponent } from './core/dashboard/dashboard.component'
import { SigninComponent } from './auth/signin/signin.component'
import { SignupComponent } from './auth/signup/signup.component'
import { UserProfileComponent } from './auth/user-profile/user-profile.component'
import { AuthGuard } from './shared/guards/auth.guard'
import { LoginGuard } from './shared/guards/login.guard'
import { BattlegroundComponent } from './battleground/battleground.component'

const routes: Routes = [
  { path: '', redirectTo: '/hero', pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'log-in', component: SigninComponent, canActivate: [LoginGuard] },
  { path: 'sign-up', component: SignupComponent },
  { path: 'user-profile/:id', component: UserProfileComponent, canActivate: [AuthGuard] },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'about', component: UsecasesComponent, children: [] },
  { path: 'hero', component: HeroComponent, canActivate: [AuthGuard] },
  { path: 'battlegrounds', component: BattlegroundComponent, canActivate: [AuthGuard] },
  { path: '**', redirectTo: '/' }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
