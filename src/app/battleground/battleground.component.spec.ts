import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'
import { BattlegroundComponent } from './battleground.component'
import { of } from 'rxjs'
import {NgbModal, NgbModalRef, NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { BattlegroundAddComponent } from './battleground-add/battleground-add.component'
import { FormsModule } from '@angular/forms'
import { NO_ERRORS_SCHEMA} from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'

describe('BattlegroundComponent', () => {
  let component: BattlegroundComponent
  let fixture: ComponentFixture<BattlegroundComponent>
  let modalService: NgbModal;
  let modalRef: NgbModalRef;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BattlegroundComponent, BattlegroundAddComponent],
      imports: [NgbModule, FormsModule, HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      schemas: [NO_ERRORS_SCHEMA]
    }).overrideModule(BrowserModule, { set: { entryComponents: [BattlegroundAddComponent] } }).compileComponents()
  }))

  beforeEach(() => {
      fixture = TestBed.createComponent(BattlegroundComponent)
      modalService = TestBed.get(NgbModal);
      modalRef = modalService.open(BattlegroundAddComponent);
      component = fixture.componentInstance
      spyOn(component.battlegroundService, 'getBattlegrounds').and.returnValue(of([{'_id':'5de82a553b06fb0017bfa37f', 'name':'karelbattleground', 'condition':'sunny', 'heroes':'[]', 'heroWon':'5de82a553b06fb0017bfa37f', 'winMessage':'You have won!', '_v':'0'}]))
      spyOn(modalService, "open").and.returnValue(modalRef);
      fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should get battlegrounds', () => {
    expect(component.battlegrounds.length).toBeGreaterThanOrEqual(1);
  })

  it('Modal opened, then Closed', (done : DoneFn) => {
    spyOn(component.battlegroundService, 'addBattleground').and.returnValue(of([{'_id':'5de82a553b06fb0017bfa37f', 'name':'karelbattleground', 'condition':'sunny', 'heroes':'[]', 'heroWon':'5de82a553b06fb0017bfa37f', 'winMessage':'You have won!', '_v':'0'}]))
    component.onClickAdd();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      modalRef.close();
      fixture.whenStable().then(() => {
        expect(component.battlegroundService.addBattleground).toHaveBeenCalled()
        done();
      });
    });
  });

  it('Modal opened, then dismissed', (done : DoneFn) => {
    component.onClickAdd();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      modalRef.dismiss();
      fixture.whenStable().then(() => {
        fixture.whenStable().then(() => {
          console.log(modalService.hasOpenModals())
          expect(modalService.hasOpenModals()).toBeFalsy()
          done();
        });
      });
    });
  });

})
