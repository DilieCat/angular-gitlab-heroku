import { Component, OnInit } from '@angular/core'
import { UseCase } from '../usecase.model'

@Component({
  selector: 'app-about-usecases',
  templateUrl: './usecases.component.html',
  styleUrls: ['./usecases.component.scss']
})
export class UsecasesComponent implements OnInit {
  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Registeren',
      description: 'Hiermee registreert een nieuwe gebruiker',
      scenario: [
        'Gebruiker vult email, password en gametag in en klikt op Login knop.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.'
      ],
      actor: 'Nieuwe gebruiker',
      precondition: 'Geen',
      postcondition: 'De actor is geregistreerd'
    },
    {
      id: 'UC-02',
      name: 'Hero aanmaken',
      description: 'Hiermee kan de gebruiker een hero aanmaken.',
      scenario: [
        'Stap 1: Kies een naam, class en power',
        'Stap 2: Upload een image hoe jouw hero eruit gaat zien.',
        'Stap 3: Kies wapens voor je hero.'
      ],
      actor: 'Speler',
      precondition: 'De actor is ingelogd',
      postcondition: 'Het doel is bereikt.'
    },
    {
      id: 'UC-03',
      name: 'Battleground verwijderen',
      description: 'Hiermee kan de administrator een battleground verwijderen.',
      scenario: [
        'Stap 1: Kies op het kopje battleground.',
        'Stap 2: Drop op het kruisje om te verwijderen.',
        'Stap 3: Kies ja bij de conformatie window.'
      ],
      actor: 'Administrator',
      precondition: 'Er zijn battlegrounds.',
      postcondition: 'Battleground is verwijderd.'
    },
    {
      id: 'UC-04',
      name: 'Wapen wijzigen bij hero.',
      description: 'Hiermee kan de speler de wapens wijzigen die de hero ge equiped heeft.',
      scenario: [
        'Stap 1: Kies op het kopje heroes.',
        'Stap 2: Drop op het wijzig knopje om te wijzigen.',
        'Stap 3: Wijzig de wapens, bijvoorbeeld een sword toevegen en een staff weghalen.'
      ],
      actor: 'Speler',
      precondition: 'De speler heeft een hero wapens..',
      postcondition: 'Wapens zijn gewijzigd..'
    },
    {
      id: 'UC-05',
      name: 'Spel starten',
      description: 'Hiermee kan de speler een gevecht starten tegen een cpu.',
      scenario: [
        'Stap 1: Kies op het kopje vechten.',
        'Stap 2: Kies de battlegroud waarop je het gevecht wil uitvoeren.',
        'Stap 3: Kies jouw hero.',
        'Stap 4: Kies de enemy hero.',
        'Stap 5: Start het gevecht.'
      ],
      actor: 'Speler',
      precondition: 'De speler heeft een hero en er is een cpu hero aangemaakt door de admin.',
      postcondition: 'De heroes gaan vechten tegen elkaar.'
    }
  ]

  constructor() {}

  ngOnInit() {}
}
