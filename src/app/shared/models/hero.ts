export interface IHero {
  _id: string
  name: string
  class: string
  power: number
  health: number
  weapons: []
}
