export class Weapon {
  constructor(
    public name: string,
    public type: string,
    public power: number,
    public description: string,
    public equiped: boolean
  ) {}
}
