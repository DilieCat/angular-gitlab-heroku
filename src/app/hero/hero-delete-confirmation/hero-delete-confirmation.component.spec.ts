import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'
import { HeroDeleteConfirmationComponent } from './hero-delete-confirmation.component'
import { NO_ERRORS_SCHEMA } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'
import { IHero } from 'src/app/shared/models/hero'
import { Weapon } from 'src/app/shared/models/weapon'

describe('HeroComponent', () => {
  let component: HeroDeleteConfirmationComponent
  let fixture: ComponentFixture<HeroDeleteConfirmationComponent>


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HeroDeleteConfirmationComponent],
      imports: [FormsModule, HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      providers: [NgbActiveModal],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents()
  }

  ))

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroDeleteConfirmationComponent)
    component = fixture.componentInstance
    const hero = fixture.debugElement.componentInstance;
    const weapons = [new Weapon("bomb", "gun", 5, "its good", true)]
    hero.hero = {name: "Karel", class: "Mage", power: 55, heatlh: 55, weapons: weapons}
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
