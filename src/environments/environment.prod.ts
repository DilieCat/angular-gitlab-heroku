export const environment = {
  production: true,
  name: '(PROD)',
  apiBaseUrl: 'https://heromania.herokuapp.com/'
}
