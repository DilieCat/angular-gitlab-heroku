import { IHero } from '../../shared/models/hero'
import { Component, OnInit, Input } from '@angular/core'
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap'

@Component({
  selector: 'app-hero-delete-confirmation',
  templateUrl: './hero-delete-confirmation.component.html',
  styleUrls: ['./hero-delete-confirmation.component.scss']
})
export class HeroDeleteConfirmationComponent implements OnInit {
  @Input() public hero

  constructor(public modal: NgbActiveModal) {}

  submitted = false

  onSubmit() {
    this.submitted = true
  }

  // TODO: Remove this when we're done

  ngOnInit() {}

  close() {
    this.modal.close(this.hero)
  }
}
