import { IHero } from '../../shared/models/hero'
import { Component, OnInit } from '@angular/core'
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { Hero } from '../../shared/models/heromodel'
import { HeroService } from 'src/app/shared/services/hero.service'

@Component({
  selector: 'app-hero-add',
  templateUrl: './hero-add.component.html',
  styleUrls: ['./hero-add.component.scss']
})
export class HeroAddComponent implements OnInit {
  public classes = this.heroService.classes

  submitted = false

  model = new Hero('Type your hero name here', this.classes[0], 1, 10, 'Type you hero description here', [])

  constructor(private heroService: HeroService, public modal: NgbActiveModal) {}

  ngOnInit() {}

  onSubmit() {
    this.submitted = true
  }

  addHero() {
    this.modal.close(this.model)
  }
}
