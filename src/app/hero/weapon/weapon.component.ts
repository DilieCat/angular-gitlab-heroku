import { Component, OnInit, Input } from '@angular/core'
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { HeroService } from 'src/app/shared/services/hero.service'
import { Weapon } from 'src/app/shared/models/weapon'

@Component({
  selector: 'app-weapon',
  templateUrl: './weapon.component.html',
  styleUrls: ['./weapon.component.scss']
})
export class WeaponComponent implements OnInit {
  @Input() public hero
  generatedPower = Math.floor(Math.random() * 101) + 1
  types = ['Sword', 'Gun', 'Hybrid', 'Chemical']

  weapon = new Weapon(
    'Type your weapon name here',
    this.types[0],
    this.generatedPower,
    'Type your description here',
    false
  )

  constructor(private heroService: HeroService, public modal: NgbActiveModal) {}

  ngOnInit() {}

  addWeapon() {
    if (this.hero.weapons.length === 0) {
      this.weapon.equiped = true
    }
    this.modal.close(this.weapon)
  }
}
