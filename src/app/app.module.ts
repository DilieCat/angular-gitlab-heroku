import { HeroEditComponent } from './hero/hero-edit/hero-edit.component'
import { HeroService } from './shared/services/hero.service'
import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './core/app/app.component'
import { UsecasesComponent } from './about/usecases/usecases.component'
import { NavbarComponent } from './core/navbar/navbar.component'
import { UsecaseComponent } from './about/usecases/usecase/usecase.component'
import { RouterModule } from '@angular/router'
import { DashboardComponent } from './core/dashboard/dashboard.component'
import { HeroComponent } from './hero/hero.component'
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome'
import { HeroDeleteConfirmationComponent } from './hero/hero-delete-confirmation/hero-delete-confirmation.component'
import { HeroAddComponent } from './hero/hero-add/hero-add.component'
import { AuthInterceptor } from './shared/interceptors/authconfig.interceptor'
import { SigninComponent } from './auth/signin/signin.component'
import { SignupComponent } from './auth/signup/signup.component'
import { UserProfileComponent } from './auth/user-profile/user-profile.component'
import { WeaponComponent } from './hero/weapon/weapon.component'
import { BattlegroundComponent } from './battleground/battleground.component'
import { BattlegroundAddComponent } from './battleground/battleground-add/battleground-add.component'

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    UsecasesComponent,
    UsecaseComponent,
    DashboardComponent,
    HeroComponent,
    HeroEditComponent,
    HeroDeleteConfirmationComponent,
    HeroAddComponent,
    SigninComponent,
    SignupComponent,
    UserProfileComponent,
    WeaponComponent,
    BattlegroundComponent,
    BattlegroundAddComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    NgbModule,
    AppRoutingModule,
    HttpClientModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    HeroService,
    NgbActiveModal,
    HttpClientModule,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    HeroEditComponent,
    HeroDeleteConfirmationComponent,
    HeroAddComponent,
    WeaponComponent,
    BattlegroundAddComponent
  ]
})
export class AppModule {}
