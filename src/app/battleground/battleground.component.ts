import { Component, OnInit } from '@angular/core'
import { BattlegroundService } from '../shared/services/battleground.service'
import { BattlegroundAddComponent } from './battleground-add/battleground-add.component'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'

@Component({
  selector: 'app-battleground',
  templateUrl: './battleground.component.html',
  styleUrls: ['./battleground.component.scss']
})
export class BattlegroundComponent implements OnInit {
  public battlegrounds = []

  constructor(public battlegroundService: BattlegroundService, private modalService: NgbModal) {}

  ngOnInit() {
    this.battlegroundService.getBattlegrounds().subscribe(data => (this.battlegrounds = data))
  }

  onClickAdd() {
    const modalRef = this.modalService.open(BattlegroundAddComponent)

    modalRef.result.then(
      battleground => {
        if (localStorage.getItem('access_token') !== undefined) {
          this.battlegroundService
            .addBattleground(battleground)
            .subscribe(newBatlleground => this.battlegrounds.push(newBatlleground))
        }
      },
      () => {}
    )
  }
}
