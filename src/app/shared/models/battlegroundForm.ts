export class BattlegroundForm {
  constructor(
    public name: string,
    public yourHeroId: string,
    public enemyHeroId: string,
    public condition: string,
    public heroWon: {},
    public winMessage: string
  ) {}
}
