import { TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'

import { BattlegroundService } from './battleground.service'

describe('BattlegroundService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule]
    })
  )

  it('should be created', () => {
    const service: BattlegroundService = TestBed.get(BattlegroundService)
    expect(service).toBeTruthy()
  })
})
