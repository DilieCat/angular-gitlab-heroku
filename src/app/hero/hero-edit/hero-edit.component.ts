import { Component, OnInit, Input } from '@angular/core'
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { HeroService } from 'src/app/shared/services/hero.service'
import { WeaponComponent } from '../weapon/weapon.component'

@Component({
  selector: 'app-hero-edit',
  templateUrl: './hero-edit.component.html',
  styleUrls: ['./hero-edit.component.scss']
})
export class HeroEditComponent implements OnInit {
  @Input() public hero

  public classes = this.heroService.classes
  public weapons = []
  submitted = false
  public equipedWeapon
  public currentWeapon

  constructor(
    private modalService: NgbModal,
    public heroService: HeroService,
    public modal: NgbActiveModal
  ) {}

  ngOnInit() {
    this.weapons = this.hero.weapons
    this.weapons.forEach(weapon => {
      if (weapon.equiped === true) {
        this.equipedWeapon = weapon._id
      }
    })
  }

  onSubmit() {
    this.submitted = true
  }

  editHero() {
    this.hero.weapons.forEach(weapon => {
      if (weapon.equiped === true) {
        weapon.equiped = false
      }
    })

    this.hero.weapons.forEach(weapon => {
      if (weapon._id === this.equipedWeapon) {
        weapon.equiped = true
      }
    })
    this.weapons = this.hero.weapons
    this.modal.close(this.hero)
  }

  addWeapon() {
    const modalRef = this.modalService.open(WeaponComponent)
    modalRef.componentInstance.hero = this.hero
    modalRef.result.then(
      weapon => {
        this.currentWeapon = weapon
        this.heroService.addWeapon(this.currentWeapon, this.hero._id).subscribe(() => {
          this.hero.weapons.push(this.currentWeapon)
          this.weapons = this.hero.weapons
        })
      },
      () => {}
    )
  }
}
