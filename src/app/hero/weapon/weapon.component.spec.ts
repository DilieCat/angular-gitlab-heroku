import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { WeaponComponent } from './weapon.component'
import { FormsModule } from '@angular/forms'
import { RouterTestingModule } from '@angular/router/testing'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'
import { Weapon } from 'src/app/shared/models/weapon'

describe('WeaponComponent', () => {
  let component: WeaponComponent
  let fixture: ComponentFixture<WeaponComponent>
  let weapons;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WeaponComponent],
      imports: [FormsModule, RouterTestingModule.withRoutes([]), HttpClientTestingModule],
      providers: [NgbActiveModal]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(WeaponComponent)
    component = fixture.componentInstance
    const hero = fixture.debugElement.componentInstance;
    weapons = [new Weapon('bomb', 'gun', 5, 'its good', false)]
    hero.hero = {name: 'Karel', class: 'Mage', power: 55, heatlh: 55, weapons}
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('Should not equip weapon if hero has more then/or one weapon.', () => {
    component.weapon = weapons[0]
    component.addWeapon()
    expect(component.weapon.equiped).toBeFalsy()
  })

  it('Should equip weapon if hero has no weapon', () => {
    component.hero.weapons = []
    component.weapon = weapons[0]
    component.addWeapon()
    expect(component.weapon.equiped).toBeTruthy()
  })
})
