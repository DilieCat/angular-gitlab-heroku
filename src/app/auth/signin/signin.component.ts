import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms'
import { AuthService } from '../../shared/services/auth.service'
import { Router } from '@angular/router'
import { NavBarService } from 'src/app/shared/services/nav-bar.service'
import { throwError } from 'rxjs/internal/observable/throwError'

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  signinForm: FormGroup
  closed = true

  constructor(
    public fb: FormBuilder,
    public authService: AuthService,
    public router: Router,
    public nav: NavBarService
  ) {
    this.signinForm = this.fb.group({
      email: new FormControl('', [
        Validators.required,
        Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$')
      ]),
      password: ['']
    })
  }

  ngOnInit() {
    this.nav.hide()
  }

  loginUser() {
    this.authService.signIn(this.signinForm.value).subscribe(
      (res: any) => {
        localStorage.setItem('access_token', res.token)
        localStorage.setItem('user_id', res._id)
        this.authService.currentUser = res._id
        this.router.navigate(['/'])
        this.nav.show()
      },
      () => {
        this.closed = false
        throwError('het kan niet')
      }
    )
  }
}
