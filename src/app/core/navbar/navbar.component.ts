import { Component, Input } from '@angular/core'
import { AuthService } from '../../shared/services/auth.service'
import { NavBarService } from '../../shared/services/nav-bar.service'

@Component({
  selector: 'app-navbar',
  template: `
    <nav *ngIf="nav.visible" class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
      <div class="container">
        <a
          class="navbar-brand"
          routerLink="/"
          [routerLinkActive]="['active']"
          [routerLinkActiveOptions]="{ exact: true }"
          >Hero Mania</a
        >
        <button
          class="navbar-toggler hidden-sm-up"
          type="button"
          (click)="isNavbarCollapsed = !isNavbarCollapsed"
          data-target="#navbarsDefault"
          aria-controls="navbarsDefault"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span class="navbar-toggler-icon"></span>
        </button>
        <div [ngbCollapse]="isNavbarCollapsed" class="collapse navbar-collapse" id="navbarsDefault">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <a
                class="nav-link"
                routerLink="about"
                [routerLinkActive]="['active']"
                [routerLinkActiveOptions]="{ exact: true }"
                (click)="isNavbarCollapsed = !isNavbarCollapsed"
                >About</a
              >
            </li>
            <li class="nav-item" data-toggle="collapse" data-target=".navbar-collapse.show">
              <a
                class="nav-link"
                routerLink="hero"
                [routerLinkActive]="['active']"
                [routerLinkActiveOptions]="{ exact: true }"
                (click)="isNavbarCollapsed = !isNavbarCollapsed"
                >My heroes</a
              >
            </li>
            <li class="nav-item" data-toggle="collapse" data-target=".navbar-collapse.show">
              <a
                class="nav-link"
                routerLink="battlegrounds"
                [routerLinkActive]="['active']"
                [routerLinkActiveOptions]="{ exact: true }"
                (click)="isNavbarCollapsed = !isNavbarCollapsed"
                >Battlegrounds</a
              >
            </li>
          </ul>
          <button (click)="logout()" *ngIf="this.authService.isLoggedIn" type="button" class="btn btn-danger">
            Logout
          </button>
        </div>
      </div>
    </nav>
  `,
  styles: [
    '.btn-link { color: rgba(255,255,255,.5); text-decoration: none; }',
    // tslint:disable-next-line: max-line-length
    '.btn-link.focus, .btn-link:focus, .btn-link.hover, .btn-link:hover { color: rgba(255,255,255,.75); text-decoration: none; box-shadow: none; }'
  ]
})
export class NavbarComponent {
  @Input() title: string
  isNavbarCollapsed = true

  constructor(public authService: AuthService, public nav: NavBarService) {}

  logout() {
    this.authService.doLogout() 
  }
}
