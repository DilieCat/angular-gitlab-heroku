import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { BattlegroundAddComponent } from './battleground-add.component'
import { FormsModule } from '@angular/forms'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { RouterTestingModule } from '@angular/router/testing'

describe('BattlegroundAddComponent', () => {
  let component: BattlegroundAddComponent
  let fixture: ComponentFixture<BattlegroundAddComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BattlegroundAddComponent],
      imports: [FormsModule, HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      providers: [NgbActiveModal]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(BattlegroundAddComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
