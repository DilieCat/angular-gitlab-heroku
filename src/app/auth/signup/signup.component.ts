import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'
import { AuthService } from '../../shared/services/auth.service'
import { Router } from '@angular/router'
import { User } from 'src/app/shared/models/user'

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  signupForm: FormGroup
  user = new User()
  isValidFormSubmitted = null

  constructor(public fb: FormBuilder, public authService: AuthService, public router: Router) {
    this.signupForm = this.fb.group({
      name: [''],
      mobile: [''],
      email: new FormControl('', [
        Validators.required,
        Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$')
      ]),
      password: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(8)])
    })
  }

  ngOnInit() {}

  registerUser() {
    this.isValidFormSubmitted = false
    if (this.signupForm.invalid) {
      return
    }
    this.isValidFormSubmitted = true
    this.authService.signUp(this.signupForm.value).subscribe(res => {
      if (res.result) {
        this.signupForm.reset()
        this.router.navigate(['log-in'])
      }
    })
  }
}
