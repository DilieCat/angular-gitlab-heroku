import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { FormsModule } from '@angular/forms'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { HeroAddComponent } from './hero-add.component'

describe('HeroAddComponent', () => {
  let component: HeroAddComponent
  let fixture: ComponentFixture<HeroAddComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HeroAddComponent],
      imports: [FormsModule, HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      providers: [NgbActiveModal]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroAddComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
