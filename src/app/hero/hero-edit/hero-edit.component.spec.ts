import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'
import { HeroEditComponent } from './hero-edit.component'
import { NO_ERRORS_SCHEMA, NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { NgbActiveModal, NgbModal, NgbModalRef, NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { IHero } from 'src/app/shared/models/hero'
import { Weapon } from 'src/app/shared/models/weapon'
import {of} from 'rxjs';
import { Hero } from 'src/app/shared/models/heromodel'
import { WeaponComponent } from '../weapon/weapon.component'
import { BrowserModule } from '@angular/platform-browser'

class MockedHeroService{

}

describe('HeroComponent', () => {
  let component: HeroEditComponent
  let fixture: ComponentFixture<HeroEditComponent>
  let hero;
  let modalService: NgbModal;
  let modalRef: NgbModalRef;



  beforeEach(async(() => {

    TestBed.configureTestingModule({
      declarations: [HeroEditComponent, WeaponComponent],
      imports: [NgbModule,FormsModule, HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      providers: [NgbActiveModal],
      schemas: [NO_ERRORS_SCHEMA]
    }).overrideModule(BrowserModule, { set: { entryComponents: [WeaponComponent] } }).compileComponents()
  }

  ))

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroEditComponent)
    modalService = TestBed.get(NgbModal);
    modalRef = modalService.open(WeaponComponent);
    component = fixture.componentInstance
    hero = fixture.debugElement.componentInstance;
    const weapons = [new Weapon('bomb', 'gun', 5, 'its good', true)]
    hero.hero = {name: 'Karel', class: 'Mage', power: 55, heatlh: 55, weapons}
    spyOn(modalService, "open").and.returnValue(modalRef);
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('If form is submitted, submitted should be true', () => {
    component.onSubmit()
    expect(component.submitted).toBeTruthy()
  })

  it('Should equipe chosen weapon', () => {
    const newWeapon = {_id: '5de82a553b06fb0017bfa37f', name: 'bomb2', type: 'gun', power: 6, description: 'its good', equiped: false};
    hero.hero.weapons.push(newWeapon)
    component.equipedWeapon = newWeapon._id;
    component.editHero()
    let wornWeapon;
    hero.hero.weapons.forEach(weapon => {
      if(weapon.equiped){
        wornWeapon = weapon
      }
    });
    expect(wornWeapon).toEqual(newWeapon)
    expect(wornWeapon).toBeTruthy();
  })

  it('If add weapon is clicked modal should open and should add weapon.', (done : DoneFn) => {
    spyOn(component.heroService, 'addWeapon').and.returnValue(of(hero.hero))
    component.addWeapon();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      modalRef.close();
      fixture.whenStable().then(() => {
        expect(component.heroService.addWeapon).toHaveBeenCalled()
        done();
      });
    });
  })

  it('If weapon is added it should add a weapon to the heroes weapon list', (done : DoneFn) => {
    spyOn(component.heroService, 'addWeapon').and.returnValue(of(hero.hero))
    component.addWeapon();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      modalRef.close();
      fixture.whenStable().then(() => {
        expect(component.weapons).toContain(component.currentWeapon)
        done();
      });
    });
  })

  it('Modal opened, then dismissed', (done : DoneFn) => {
    component.addWeapon();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      modalRef.dismiss();
      fixture.whenStable().then(() => {
        fixture.whenStable().then(() => {
          expect(modalService.hasOpenModals()).toBeFalsy()
          done();
        });
      });
    });
  });

})
