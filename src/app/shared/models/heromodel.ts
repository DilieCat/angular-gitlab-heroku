export class Hero {
  constructor(
    public name: string,
    public classes: string,
    public power: number,
    public health: number,
    public description: string,
    public weapon: [],
    public _id?: number
  ) {}
}
