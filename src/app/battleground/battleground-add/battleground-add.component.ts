import { Component, OnInit } from '@angular/core'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'
import { BattlegroundService } from 'src/app/shared/services/battleground.service'
import { HeroService } from 'src/app/shared/services/hero.service'
import { AuthService } from 'src/app/shared/services/auth.service'
import { BattlegroundForm } from 'src/app/shared/models/battlegroundForm'
import { Battleground } from 'src/app/shared/models/battleground'

@Component({
  selector: 'app-battleground-add',
  templateUrl: './battleground-add.component.html',
  styleUrls: ['./battleground-add.component.scss']
})
export class BattlegroundAddComponent implements OnInit {
  public conditions = this.battlegroundService.condition
  public currentUserId = this.authService.currentUser
  public yourHeroes = []
  public enemyHeroes = []
  public yourFirstHero
  public enemyFirstHero
  public yourWeapon
  public enemyWeapon
  public model = new BattlegroundForm(
    'Type the name of the battleground here',
    '',
    '',
    this.conditions[0],
    {},
    ''
  )

  constructor(
    public modal: NgbActiveModal,
    private battlegroundService: BattlegroundService,
    private heroService: HeroService,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.heroService.getUserHero(this.currentUserId).subscribe(data => {
      data.forEach(element => {
        if (element.weapons.length > 0) {
          this.yourHeroes.push(element)
        }
      })
      this.yourFirstHero = this.yourHeroes[0]._id
    })
    this.heroService.getHeroExceptFromUser(this.currentUserId).subscribe(data => {
      data.forEach(element => {
        if (element.weapons.length > 0) {
          this.enemyHeroes.push(element)
        }
      })
      this.enemyFirstHero = this.enemyHeroes[0]._id
    })
  }

  addBattleground() {
    this.model.enemyHeroId = this.enemyFirstHero
    this.model.yourHeroId = this.yourFirstHero
    this.modal.close(this.model)
  }
}
